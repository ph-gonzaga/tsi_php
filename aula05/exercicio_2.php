<?php

//Operadores === e !==

$nome = 'Luiz Fernando Albertin Bono Milan';

$posicao = strpos( $nome, 'Luiz');

if( $posicao !== false ){

	echo "Encontrado na posição $posicao!!";
}

echo '<br><br>';

//Vetor
$alunos = array(0 => array(	'nome' => 'Luiz ',
							'bitbucket' => 'https://bi.'),
				1 => array(	'nome' => 'Carlos',
							'bitbucket' => 'https://bi.'),
				2 => array(	'nome' => 'Aline',
							'bitbucket' => 'https://bi.'),
				3 => array(	'nome' => 'Maria',
							'bitbucket' => 'https://bi.'),
										);

//Tabela linda!
echo '<table border="1">
		<thead>
		<th>
			Nome
		</th>
		<th>
			Bitbucket
		</th>
		</thead>';

$cor = null;

foreach ($alunos as $ind => $linha) {
	
	$cor = $cor == 'gray' ? 'white' : 'gray';

	echo "	<tr bgcolor='$cor'>
				<td>
					{$linha['nome']}
				</td>
				<td>
					{$linha['bitbucket']}
				</td>
			</tr>";	
}

echo '</table><br><br>';


//Tabela linda, mas com IF!
echo '<table border="1">
		<thead>
		<th>
			Nome
		</th>
		<th>
			Bitbucket
		</th>
		</thead>';

$cor = null;

foreach ($alunos as $ind => $linha) {
	
	if( $cor == 'gray' ){

		$cor = 'white';

	}else{

		$cor = 'gray';
	}

	echo "	<tr bgcolor='$cor'>
				<td>
					{$linha['nome']}
				</td>
				<td>
					{$linha['bitbucket']}
				</td>
			</tr>";	
}

echo '</table><br><br>';

