<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Menu</title>
	<link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
	<?php

	session_start();

	$usuario = $_SESSION['usuario'];

	if(!isset($_SESSION['usuario'])){
		header('Location: home.php');
	}

	include 'conexao.php';

	$sql = "SELECT nivel_usuario FROM usuario WHERE email_usuario = '$usuario' and status='Ativo'";
	$buscar = mysqli_query($conexao,$sql);
	$array = mysqli_fetch_array($buscar);

	$nivel = $array['nivel_usuario'];

	?>

	<div class="container" style="margin-top: 20px">

		<div class="row">
			<?php

			if(($nivel == 1)||($nivel == 2)){

				?>
				<div class="col-sm-6">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Adicionar Produto</h5>
							<p class="card-text">Opção para adicionar produtos no estoque</p>
							<a href="adicionar_produto.php" class="btn btn-primary">Cadastrar Produto</a>
						</div>
					</div>
				</div>

			<?php } ?>

			<div class="col-sm-6">
			<div class="card">
			<div class="card-body">
			<h5 class="card-title">Lista de Produtos</h5>
			<p class="card-text">Visualizar, editar e excluir produtos</p>
			<a href="listar_produto.php" class="btn btn-primary">Produtos</a>
			</div>
			</div>
			</div>

			</div>

			</div>


			<script type="text/javascript" src="js/bootstrap.js">

			</script>
			</body>
			</html>