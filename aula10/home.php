<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
	<title>Menu</title>
	<link rel="stylesheet" href="css/bootstrap.css">
	<style type="text/css">
		#tamanho{
			width: 350px;
			margin-top: 100px;
			border-radius: 15px;
			border:2px solid #f3f3f3;
			padding: 10px;
		}

	</style>
</head>
<body>

	<div class="container" id="tamanho">
		<center>
			<img src="img/login.png" width="175px" height="175px">
		</center>
		<form action="index1.php" method="post">
			<div class="form-group">
				<label>Usuario</label>
				<input type="text" class="form-control" name="usuario" placeholder="Usuário" autocomplete="off" required >
			</div>

			<div class="form-group">
				<label>Senha</label>
				<input type="password" class="form-control" name="senha" placeholder="Senha" autocomplete="off" required >
			</div>

			<div style="text-align: right;">
				<button type="submit" id="botao" class="btn btn-danger btn-sm">Entrar</button>
			</div>
		</form>
	</div>

</body>
<script type="text/javascript" src="js/bootstrap.js"></script>
</html>