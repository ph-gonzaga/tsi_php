<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Lista de Produtos</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <script src="https://kit.fontawesome.com/1d6f563437.js" crossorigin="anonymous"></script>
</head>

<style>
.searchbar {
    margin-bottom: auto;
    margin-top: 50px;
    height: 60px;
    background-color: #353b48;
    border-radius: 30px;
    padding: 10px;
}

.search_input {
    color: white;
    border: 0;
    outline: 0;
    background: none;
    width: 0;
    caret-color: transparent;
    line-height: 40px;
    transition: width 0.4s linear;
}

.searchbar:hover>.search_input {
    padding: 0 10px;
    width: 450px;
    caret-color: red;
    transition: width 0.4s linear;
}

.searchbar:hover>.search_icon {
    background: white;
    color: #e74c3c;
}

.search_icon {
    height: 40px;
    width: 40px;
    float: right;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 50%;
    color: white;


    .active-pink-2 input[type=text]:focus:not([readonly]) {
        border-bottom: 1px solid #f48fb1;
        box-shadow: 0 1px 0 0 #f48fb1;
    }

    .active-pink input[type=text] {
        border-bottom: 1px solid #f48fb1;
        box-shadow: 0 1px 0 0 #f48fb1;
    }

    .active-purple-2 input[type=text]:focus:not([readonly]) {
        border-bottom: 1px solid #ce93d8;
        box-shadow: 0 1px 0 0 #ce93d8;
    }

    .active-purple input[type=text] {
        border-bottom: 1px solid #ce93d8;
        box-shadow: 0 1px 0 0 #ce93d8;
    }

    .active-cyan-2 input[type=text]:focus:not([readonly]) {
        border-bottom: 1px solid #4dd0e1;
        box-shadow: 0 1px 0 0 #4dd0e1;
    }

    .active-cyan input[type=text] {
        border-bottom: 1px solid #4dd0e1;
        box-shadow: 0 1px 0 0 #4dd0e1;
    }

    .active-cyan .fa,
    .active-cyan-2 .fa {
        color: #4dd0e1;
    }

    .active-purple .fa,
    .active-purple-2 .fa {
        color: #ce93d8;
    }

    .active-pink .fa,
    .active-pink-2 .fa {
        color: #f48fb1;
    }
}
</style>

<body>

    <?php

	session_start();

	$usuario = $_SESSION['usuario'];

	if(!isset($_SESSION['usuario'])){
		header('Location: home.php');
	}

	include 'conexao.php';

	$sql = "SELECT nivel_usuario FROM usuario WHERE email_usuario = '$usuario' and status='Ativo'";
	$buscar = mysqli_query($conexao,$sql);
	$array = mysqli_fetch_array($buscar);

	$nivel = $array['nivel_usuario'];

	?>

    <div class="container" id="tamanhoContainer" style="margin-top: 40px">
        <h4>Listar Produtos</h4>

        <form name="searchform" method="post" action="">
            <i class="fas fa-search"></i><input type="text" name="pesquisar" />
            <input type="submit" name="Enviar" value="Busca" />
        </form>


        <br>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Nro Produto</th>
                    <th scope="col">Nome Produto</th>
                    <th scope="col">Categoria</th>
                    <th scope="col">Quantidade</th>
                    <th scope="col">Fornecedor</th>
                    <th scope="col">Ação</th>
                </tr>
            </thead>

            <?php

			include 'conexao.php';

			if (isset($_POST['pesquisar'])){
			$pesquisar = $_POST['pesquisar'];
			$sql = "SELECT * FROM estoque WHERE nomeproduto LIKE '%$pesquisar%' OR nroproduto LIKE '%$pesquisar%'";
			$busca = mysqli_query($conexao, $sql);
			}else {
			$sql = "SELECT * FROM estoque";
			$busca = mysqli_query($conexao, $sql);
			}


			while ($array = mysqli_fetch_array($busca)){
				$id_estoque = $array['id_estoque'];
				$nroproduto = $array['nroproduto'];
				$nomeproduto = $array['nomeproduto'];
				$categoria = $array['categoria'];
				$quantidade = $array['quantidade'];
				$fornecedor = $array['fornecedor'];

				?>
            <tr>
                <td><?php echo $nroproduto ?></td>
                <td><?php echo $nomeproduto ?></td>
                <td><?php echo $categoria ?></td>
                <td><?php echo $quantidade ?></td>
                <td><?php echo $fornecedor ?></td>
                <td>

                    <?php
						if(($nivel==1)||($nivel==2)){

							?>

                    <a class="btn btn-warning btn-sm" style="color:white"
                        href="editar_produto.php?id=<?php echo $id_estoque ?>" role="button"><i
                            class="far fa-edit"></i>&nbsp;Editar</a>

                    <?php }

						if($nivel==1){

							?>

                    <a class="btn btn-danger btn-sm" style="color:white"
                        href="deletar_produto.php?id=<?php echo $id_estoque ?>" role="button"><i
                            class="fas fa-trash"></i>&nbsp;Deletar</a>
                    <?php } ?>

                </td>


            </tr>
            <?php } ?>

        </table>

    </div>
</body>

</html>